package org.elm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class Main {
	
	public static Hashtable<String, Vector<String>> bib=
		new Hashtable<String, Vector<String>>();
	
	public static void instruciones(){
		System.out.println("org.elm.Main directorio");
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 * @throws ZipException 
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args)  {
		if(args.length!=1){
			instruciones();
			System.exit(0);
		}
		File f=new File(args[0]);
		try {
			ObjectInputStream ois=new ObjectInputStream(
					new FileInputStream(
							new File(f,"Biblioteca.job")
							));
			bib=(Hashtable<String, Vector<String>>) ois.readObject();
			ois.close();
		} catch (FileNotFoundException e1) {
		} catch (Exception e1) {
			e1.printStackTrace();
			System.exit(-1);
		}
		processDirectory(f);
		try {
			ObjectOutputStream oos=new ObjectOutputStream(
					new FileOutputStream(new File(f,"Biblioteca.job"))
					);
			oos.writeObject(bib);
			oos.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		Enumeration<String> keys=bib.keys();
		while(keys.hasMoreElements()){
			String libro=keys.nextElement();
			Vector<String> cnt=bib.get(libro);
			for(int i=0;i<cnt.size();i++)
				System.out.println(libro+":"+cnt.elementAt(i));
		}
	}

	private static void processDirectory(File f) {
		File listado[]=f.listFiles();
		for(int i=0;i<listado.length;i++){
			File ef=listado[i];
			if(ef.isDirectory()){
				processDirectory(ef);
			}else if(ef.getName().toLowerCase().endsWith(".zip")){
				processFile(ef);
			}
		}
	}

	private static int numbook=0;
	private static void processFile(File ef) {
		ZipFile zf=null;
		try {
			zf = new ZipFile(ef);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		Vector<String> bookContent=new Vector<String>();
		try {
			bib.put(ef.getCanonicalPath(),bookContent);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		if(++numbook%100==0) System.out.print("*");
		Enumeration<? extends ZipEntry> eze= zf.entries();
		while(eze.hasMoreElements()){
			ZipEntry zeBookContent=eze.nextElement();
			bookContent.add(zeBookContent.getName());
		}
	}
}
